from django.contrib.auth.models import User
from .models import Reviews
from django import forms


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]

class ReviewForm(forms.ModelForm):

    class Meta:
        model = Reviews
        fields = ['comments',
                  ]