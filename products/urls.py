from django.conf.urls import url
from .views import *

app_name = 'products'

urlpatterns = [
    url(r'^$', CategoryList.as_view(), name='categories'),
    url(r'^category/(?P<slug>[-\w]+)/$', CategoryDetail.as_view(), name='category_detail'),
    url(r'^login/$', login_user, name='login'),
    url(r'^register/$', UserForm.as_view(), name='register'),
    url(r'^logout/$', logout_user, name='logout'),
    url(r'^product/(?P<slug>[-\w]+)/$', ProductDetail.as_view(), name='product_detail'),
    url(r'^(?P<slug>[-\w]+)/(?P<user>[-\w]+)/$', comment, name='comment'),
    url(r'^thanks/$', thanks, name='thanks'),

]


