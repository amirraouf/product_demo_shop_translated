# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-24 15:35
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('description', models.CharField(blank=True, max_length=300)),
                ('slug', models.SlugField(blank=True)),
            ],
            options={
                'db_table': 'products_categories',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Merchant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('logo', models.FileField(upload_to=b'')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_en', models.CharField(default=0, max_length=255)),
                ('title_ar', models.CharField(max_length=255, null=True)),
                ('slug', models.SlugField(allow_unicode=True, blank=True)),
                ('specs', django.contrib.postgres.fields.jsonb.JSONField()),
                ('category', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='products.Category')),
                ('merchant', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='products.Merchant')),
            ],
        ),
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comments', models.CharField(default=0, max_length=510)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.Product')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterOrderWithRespectTo(
            name='product',
            order_with_respect_to='title_en',
        ),
        migrations.AlterOrderWithRespectTo(
            name='category',
            order_with_respect_to='title',
        ),
    ]
