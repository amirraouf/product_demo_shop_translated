from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import HttpResponseRedirect, redirect, get_object_or_404
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import FormView
from django.views.generic import ListView
from amirshop.settings import LANGUAGES
from .models import *
from .forms import UserForm, ReviewForm


# Create your views here.


class UserForm(FormView):
    form_class = UserForm
    template_name = "register.html"


    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
            return HttpResponseRedirect('/')
        return HttpResponseRedirect('/')


class CategoryList(ListView):
    model = Category
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(CategoryList, self).get_context_data(**kwargs)
        context['LANGUAGES'] = LANGUAGES
        return context


class CategoryDetail(ListView):
    model = Product
    template_name = "category.html"


class ProductDetail(DetailView):
    model = Product
    template_name = "product_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ProductDetail, self).get_context_data(**kwargs)
        context['form'] = ReviewForm()
        return context


def comment(request, slug, user):
    product = get_object_or_404(Product, slug=slug)
    from django.contrib.auth.models import User
    user = get_object_or_404(User, username=user)
    print [user, product]
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.user = user
            review.product = product
            review.save()

            return HttpResponseRedirect('/thanks/')

    else:
        form = ReviewForm()
        return render(request, 'product_detail.html', {'form': form})


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                category = Category.objects.all()
                return render(request, 'index.html', {'object_list': category})
            else:
                return render(request, 'login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'login.html', {'error_message': 'Invalid login'})
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/')


def thanks(request):
    return render(request, 'thanks.html')