from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.text import slugify
from django.conf import settings


class Category(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=300, blank=True)
    slug = models.SlugField(blank=True)

    class Meta:
        order_with_respect_to = 'title'
        verbose_name_plural = "Categories"
        db_table = 'products_categories'

    def __str__(self):
        return u'%s' % self.title

    def clean(self):
        return self.clean_fields()

    def get_absolute_url(self):
        self.slug = slugify(self.title)
        self.save()
        return reverse('products:category_detail', kwargs={'slug': self.slug})


class Merchant(models.Model):
    title = models.CharField(max_length=100)
    logo = models.FileField()


class Product(models.Model):
    title_en = models.CharField(max_length=255, null=False, default=0)
    title_ar = models.CharField(max_length=255, null=True)
    slug = models.SlugField(allow_unicode=True, blank=True)
    specs = JSONField()
    category = models.ForeignKey(Category, default=0)
    merchant = models.ForeignKey(Merchant, null=False, default=0)

    def __str__(self):
        return self.title_en

    def get_absolute_url(self):
        self.slug = slugify(self.title_en)
        self.save()
        return reverse('products:product_detail', kwargs={'slug': self.slug})

    class Meta:
        order_with_respect_to = 'title_en'


class Reviews(models.Model):
    product = models.ForeignKey(Product)
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    comments = models.CharField(max_length=510, null=False, default="")
    approved_comment = models.BooleanField(default=False)

    def __str__(self):
        return self.comments
